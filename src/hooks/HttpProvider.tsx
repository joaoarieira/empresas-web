import { ReactNode } from 'react';
import { useNavigate } from 'react-router-dom';
import { CachePolicies, IncomingOptions, Provider } from 'use-http';
import { useAuth } from './AuthContext';

interface IHttpProviderProps {
  children: ReactNode;
}

export function HttpProvider({ children }: IHttpProviderProps): JSX.Element {
  const { authenticated, oAuth } = useAuth();
  const navigate = useNavigate();

  const optionsProvider = {
    cachePolicy: CachePolicies.NO_CACHE,
    interceptors: {
      request: ({ options }) => {
        if (authenticated) {
          const newOptions = options;
          newOptions.headers = {
            ...options.headers,
            ...oAuth,
          };
          return newOptions;
        }
        return options;
      },
      // eslint-disable-next-line @typescript-eslint/no-explicit-any
      response: ({ response }: any) => {
        const res = response;

        if (res.status === 401) navigate('/logout');

        return res;
      },
    },
  } as IncomingOptions;

  return (
    <Provider url={process.env.REACT_APP_BACKEND_URL} options={optionsProvider}>
      {children}
    </Provider>
  );
}

export default HttpProvider;
