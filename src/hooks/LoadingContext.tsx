import {
  createContext,
  ReactElement,
  ReactNode,
  useContext,
  useMemo,
  useState,
} from 'react';

interface ILoadingProviderProps {
  children: ReactNode;
}

interface ILoadingContextData {
  loading: boolean;
  setLoading: React.Dispatch<React.SetStateAction<boolean>>;
}

const LoadingContext = createContext<ILoadingContextData>(
  {} as ILoadingContextData,
);

export function LoadingProvider({
  children,
}: ILoadingProviderProps): ReactElement {
  const [loading, setLoading] = useState(false);
  const memoizedContextValue = useMemo<ILoadingContextData>(
    () => ({
      loading,
      setLoading,
    }),
    [loading, setLoading],
  );

  return (
    <LoadingContext.Provider value={memoizedContextValue}>
      {children}
    </LoadingContext.Provider>
  );
}

export const useLoading = (): ILoadingContextData => {
  return useContext(LoadingContext);
};

export default LoadingProvider;
