import {
  createContext,
  ReactElement,
  ReactNode,
  useCallback,
  useContext,
  useEffect,
  useMemo,
  useState,
} from 'react';
import { useNavigate } from 'react-router-dom';
import useFetch, { CachePolicies } from 'use-http';
import { useLoading } from './LoadingContext';

interface IOAuth {
  client?: string;
  uid?: string;
  'access-token'?: string;
}

interface IAuthProviderProps {
  children: ReactNode;
}

interface IAuthContextData {
  authenticated: boolean;
  oAuth: IOAuth;
  handleLogin: (email: string, password: string) => Promise<boolean>;
  handleLogout: () => void;
}

const AuthContext = createContext<IAuthContextData>({} as IAuthContextData);

export function AuthProvider({ children }: IAuthProviderProps): ReactElement {
  const [authenticated, setAuthenticated] = useState(false);
  const [oAuth, setOAuth] = useState({
    client: undefined,
    uid: undefined,
    'access-token': undefined,
  } as IOAuth);

  const { setLoading } = useLoading();

  const { post, response } = useFetch(process.env.REACT_APP_BACKEND_URL, {
    cachePolicy: CachePolicies.NO_CACHE,
  });

  const navigate = useNavigate();

  const handleLogin = useCallback(
    async (email: string, password: string): Promise<boolean> => {
      setLoading(true);
      await post('/users/auth/sign_in', { email, password });

      if (response.ok) {
        setAuthenticated(true);
        const newOAuth = {
          'access-token': response.headers.get('access-token') ?? '',
          uid: response.headers.get('uid') ?? '',
          client: response.headers.get('client') ?? '',
        };

        setOAuth(newOAuth);

        localStorage.setItem('@EmpresasWeb:oAuth', JSON.stringify(newOAuth));
      } else {
        setAuthenticated(false);
        localStorage.removeItem('@EmpresasWeb:oAuth');
      }
      setLoading(false);
      return response.ok ?? false;
    },
    [post, response, setLoading],
  );

  const handleLogout = useCallback(() => {
    setAuthenticated(false);
    localStorage.removeItem('@EmpresasWeb:oAuth');
    navigate('/');
  }, [navigate]);

  const getTokenFromLocalStorage = useCallback(() => {
    const tokenLocalStorage = localStorage.getItem('@EmpresasWeb:oAuth');
    if (tokenLocalStorage) {
      const newOAuth = JSON.parse(tokenLocalStorage);

      setAuthenticated(true);
      setOAuth(newOAuth);
    }
  }, []);

  useEffect(() => {
    getTokenFromLocalStorage();
  }, [getTokenFromLocalStorage]);

  const memoizedContextValue = useMemo<IAuthContextData>(
    () => ({
      authenticated,
      oAuth,
      handleLogin,
      handleLogout,
    }),
    [authenticated, oAuth, handleLogin, handleLogout],
  );

  return (
    <AuthContext.Provider value={memoizedContextValue}>
      {children}
    </AuthContext.Provider>
  );
}

export const useAuth = (): IAuthContextData => {
  return useContext(AuthContext);
};

export default AuthProvider;
