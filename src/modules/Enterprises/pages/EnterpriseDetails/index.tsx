import { useCallback, useEffect, useState } from 'react';
import { BiArrowBack } from 'react-icons/bi';
import { useParams } from 'react-router-dom';
import { GenericContainer } from '../../../../components/GenericContainer';
import { Header } from '../../../../components/Header';
import { IEnterprise, useEnterprises } from '../../hooks/EnterprisesContext';
import {
  Container,
  FakeImg,
  EnterpriseDescription,
  BackButton,
  EnterpriseName,
} from './styles';

export function EnterpriseDetails() {
  const [enterprise, setEnterprise] = useState<IEnterprise>();

  const { id } = useParams();
  const { enterprises } = useEnterprises();

  const handleClickBackButton = useCallback(() => {
    window.history.back();
  }, []);

  const findEnterprise = useCallback(() => {
    if (id) {
      const newEnterprise = enterprises.find(
        item => item.id === parseInt(id, 10),
      );
      if (newEnterprise) {
        setEnterprise(newEnterprise);
      } else {
        handleClickBackButton();
      }
    } else {
      handleClickBackButton();
    }
  }, [enterprises, handleClickBackButton, id]);

  useEffect(() => {
    findEnterprise();
  }, [findEnterprise]);

  return (
    <article>
      <Header>
        <BackButton type="button" onClick={handleClickBackButton}>
          <BiArrowBack size={35} color="var(--white)" />
        </BackButton>
        <EnterpriseName>{enterprise?.enterprise_name}</EnterpriseName>
      </Header>
      <GenericContainer>
        {id && (
          <Container>
            <FakeImg>{enterprise?.enterprise_name.slice(0, 2)}</FakeImg>
            <EnterpriseDescription>
              {enterprise?.description}
            </EnterpriseDescription>
          </Container>
        )}
      </GenericContainer>
    </article>
  );
}

export default EnterpriseDetails;
