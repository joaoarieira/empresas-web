import styled from 'styled-components';

export const Container = styled.div`
  padding: 48px 75px;
  background: var(--white);
  border-radius: 5px;

  display: grid;
  grid-template-columns: 1fr;
  row-gap: 50px;

  @media (max-width: 900px) {
    padding: 25px 30px;
  }
`;

export const FakeImg = styled.div`
  width: 100%;
  height: 300px;
  background: #7dc075;
  color: var(--white);
  display: flex;
  justify-content: center;
  align-items: center;
  font-size: 4rem;
  font-weight: bold;

  @media (max-width: 900px) {
    margin: 0 auto;
    max-width: 300px;
    height: 200px;
  }
`;

export const EnterpriseDescription = styled.p`
  font-size: 2.125rem;
  color: var(--grey-500);

  @media (max-width: 900px) {
    font-size: 1.5rem;
  }
`;

export const BackButton = styled.button`
  background-color: transparent;
  border: none;
`;

export const EnterpriseName = styled.h1`
  color: white;
  text-transform: uppercase;
  font-size: 2.125rem;
  margin-left: 80px;

  @media (max-width: 900px) {
    font-size: 1.5rem;
    margin-left: 60px;
  }

  @media (max-width: 425px) {
    font-size: 1.5rem;
    margin-left: 20px;
  }
`;
