import styled from 'styled-components';

export const ListItem = styled.li`
  list-style: none;
  cursor: pointer;

  & + & {
    margin-top: 1.875rem;
  }
`;

export const FakeImg = styled.div`
  width: 100%;
  height: 160px;
  background: #7dc075;
  color: var(--white);
  display: flex;
  justify-content: center;
  align-items: center;
  font-size: 4rem;
  font-weight: bold;

  @media (max-width: 900px) {
    margin: 0 auto;
    max-width: 300px;
  }
`;

export const Container = styled.div`
  padding: 27px 30px;
  background: var(--white);
  border-radius: 5px;

  display: grid;
  grid-template-columns: 0.35fr 0.75fr;
  column-gap: 40px;

  @media (max-width: 900px) {
    padding: 27px 22px;
    grid-template-columns: 1fr;
  }
`;

export const EnterpriseName = styled.h4`
  font-size: 1.875rem;
  color: var(--indigo-900);

  @media (max-width: 900px) {
    font-size: 1.5rem;
  }
`;

export const EnterpriseData = styled.article`
  display: flex;
  align-items: flex-start;
  justify-content: center;
  flex-direction: column;
  row-gap: 0.5rem;

  @media (max-width: 900px) {
    margin-top: 1.5rem;
  }
`;

export const EnterpriseTypeName = styled.p`
  font-size: 1.5rem;
  color: var(--grey-500);

  @media (max-width: 900px) {
    font-size: 1.25rem;
  }
`;

export const EnterpriseCountry = styled.p`
  font-size: 1.125rem;
  color: var(--grey-500);

  @media (max-width: 900px) {
    font-size: 1rem;
  }
`;
