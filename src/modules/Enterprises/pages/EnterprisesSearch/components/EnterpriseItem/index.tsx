import { useCallback } from 'react';
import { useNavigate } from 'react-router-dom';
import { IEnterprise } from '../../../../hooks/EnterprisesContext';

import {
  Container,
  EnterpriseName,
  ListItem,
  EnterpriseData,
  EnterpriseTypeName,
  EnterpriseCountry,
  FakeImg,
} from './styles';

interface IEnterpriseItemProps {
  enterprise: IEnterprise;
}

export function EnterpriseItem({
  enterprise,
}: IEnterpriseItemProps): JSX.Element {
  const navigate = useNavigate();

  const handleClickListItem = useCallback(() => {
    navigate(`${enterprise.id}`);
  }, [enterprise.id, navigate]);

  return (
    <ListItem onClick={handleClickListItem}>
      <Container>
        <FakeImg>{enterprise.enterprise_name.slice(0, 2)}</FakeImg>

        <EnterpriseData>
          <EnterpriseName>{enterprise.enterprise_name}</EnterpriseName>

          <EnterpriseTypeName>
            {enterprise.enterprise_type.enterprise_type_name}
          </EnterpriseTypeName>

          <EnterpriseCountry>{enterprise.country}</EnterpriseCountry>
        </EnterpriseData>
      </Container>
    </ListItem>
  );
}
