import { IEnterprise } from '../../../../hooks/EnterprisesContext';
import { EnterpriseItem } from '../EnterpriseItem';

interface IEnterpriseListProps {
  enterprises: IEnterprise[];
}

export function EnterpriseList({
  enterprises,
}: IEnterpriseListProps): JSX.Element {
  return (
    <ul>
      {enterprises.map(enterprise => (
        <EnterpriseItem key={enterprise.id} enterprise={enterprise} />
      ))}
    </ul>
  );
}
