import { useCallback, useEffect, useState } from 'react';
import { HiOutlineSearch } from 'react-icons/hi';
import { MdClear } from 'react-icons/md';
import { SyncLoader } from 'react-spinners';
import useFetch from 'use-http';

import logoImg from '../../../../assets/logo-white.png';
import { GenericContainer } from '../../../../components/GenericContainer';
import { Header } from '../../../../components/Header';
import { useEnterprises } from '../../hooks/EnterprisesContext';
import { EnterpriseList } from './components/EnterpriseList';
import {
  SearchWrapper,
  FirstSearchContainer,
  LogoWrapper,
  Spacer,
  CenteredText,
  SearchInput,
  SearchInputWrapper,
} from './styles';

export function EnterprisesSearch(): JSX.Element {
  const [query, setQuery] = useState('');
  const [delayedQuery, setDelayedQuery] = useState('');
  const [noResults, setNoResults] = useState(false);
  const [isWaitingForUserFillInput, setIsWaitingForUserFillInput] =
    useState(false);

  const { enterprises, setEnterprises, firstSearch, setFirstSearch } =
    useEnterprises();

  const { get, response, loading } = useFetch('enterprises');

  const handleClickSearch = useCallback(
    () => setFirstSearch(false),
    [setFirstSearch],
  );

  const handleChangeSearchInput = useCallback(({ target: { value } }) => {
    setQuery(value);
  }, []);

  const handleClearSearchInput = useCallback(() => {
    setQuery('');
  }, []);

  const fetchEnterpriseData = useCallback(async () => {
    await get(`?name=${delayedQuery}`);

    if (response.ok) {
      setEnterprises(response.data.enterprises);
      setNoResults(response.data.enterprises.length === 0);
    }
  }, [get, delayedQuery, response, setEnterprises]);

  useEffect(() => {
    const timeOutId = setTimeout(() => setDelayedQuery(query), 1000);
    return () => clearTimeout(timeOutId);
  }, [query]);

  useEffect(() => {
    if (query === delayedQuery) {
      setIsWaitingForUserFillInput(false);
      if (query.length > 0 && !isWaitingForUserFillInput) {
        fetchEnterpriseData();
      }
    } else if (isWaitingForUserFillInput !== true) {
      setIsWaitingForUserFillInput(true);
    }
  }, [delayedQuery, fetchEnterpriseData, isWaitingForUserFillInput, query]);

  return (
    <div>
      <Header>
        {firstSearch ? (
          <FirstSearchContainer>
            <Spacer />

            <LogoWrapper>
              <img src={logoImg} alt="Logo" style={{ width: '15rem' }} />
            </LogoWrapper>

            <SearchWrapper>
              <button type="button" onClick={handleClickSearch}>
                <HiOutlineSearch color="var(--white)" size={32} />
              </button>
            </SearchWrapper>
          </FirstSearchContainer>
        ) : (
          <SearchInputWrapper>
            <SearchInput
              autoFocus
              placeholder="Pesquisar"
              value={query}
              onChange={handleChangeSearchInput}
              startIcon={
                !isWaitingForUserFillInput && !loading ? (
                  <HiOutlineSearch color="var(--white)" size={32} />
                ) : (
                  <SyncLoader
                    size={6}
                    css="position: relative; top: 5px"
                    color="var(--white)"
                  />
                )
              }
              endIcon={
                <MdClear
                  color="var(--white)"
                  size={32}
                  cursor="pointer"
                  onClick={handleClearSearchInput}
                />
              }
              containerStyles={{
                borderColor: 'var(--white)',
                paddingBottom: '45px',
              }}
            />
          </SearchInputWrapper>
        )}
      </Header>

      <GenericContainer>
        {firstSearch && (
          <CenteredText>Clique na busca para iniciar.</CenteredText>
        )}

        {noResults && (
          <CenteredText>
            Nenhuma empresa foi encontrada para a busca realizada.
          </CenteredText>
        )}

        {!firstSearch && !noResults && (
          <section>
            <EnterpriseList enterprises={enterprises} />
          </section>
        )}
      </GenericContainer>
    </div>
  );
}

export default EnterprisesSearch;
