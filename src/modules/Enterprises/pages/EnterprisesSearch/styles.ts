import styled from 'styled-components';
import { Input } from '../../../../components/Input';

export const FirstSearchContainer = styled.div`
  display: grid;
  grid-template-columns: repeat(3, 1fr);
  margin: 0 auto;
  width: 100%;
`;

export const Spacer = styled.span`
  flex-basis: 33.33%;
`;

export const LogoWrapper = styled.span`
  flex-basis: 33.33%;
  display: flex;
  justify-content: center;
  align-items: center;
`;

export const SearchWrapper = styled.span`
  display: flex;
  justify-content: flex-end;
  flex-basis: 33.33%;

  button {
    background-color: transparent;
    border: none;

    path {
      stroke-width: 2.5px;
    }
  }
`;

export const CenteredText = styled.div`
  display: flex;
  justify-content: center;
  align-items: center;
  height: calc(100vh - 9.5rem);
  font-size: 2rem;
  color: var(--grey-800);
  text-align: center;

  @media (max-width: 900px) {
    font-size: 1.5rem;
  }
`;

export const SearchInputWrapper = styled.span`
  width: 100%;
  margin-top: 2.5rem;

  @media (max-width: 900px) {
    margin-top: 1rem;
  }
`;

export const SearchInput = styled(Input)`
  caret-color: var(--white);
  color: var(--white);
  font-size: 2.125rem;
  width: calc(100% - 50px);

  &::placeholder {
    color: var(--rouge);
    font-size: 2.125rem;
  }
`;

export const ClearButton = styled.button`
  background-color: transparent;
`;
