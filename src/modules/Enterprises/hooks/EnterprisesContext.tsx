import {
  createContext,
  ReactElement,
  ReactNode,
  useContext,
  useMemo,
  useState,
} from 'react';

interface IEnterpriseType {
  id: string;
  enterprise_type_name: string;
}

export interface IEnterprise {
  id: number;
  enterprise_name: string;
  description: string;
  country: string;
  enterprise_type: IEnterpriseType;
}

interface IEnterprisesProviderProps {
  children: ReactNode;
}

interface IEnterprisesContextData {
  enterprises: IEnterprise[];
  setEnterprises: React.Dispatch<React.SetStateAction<IEnterprise[]>>;
  firstSearch: boolean;
  setFirstSearch: React.Dispatch<React.SetStateAction<boolean>>;
}

const EnterprisesContext = createContext<IEnterprisesContextData>(
  {} as IEnterprisesContextData,
);

export function EnterprisesProvider({
  children,
}: IEnterprisesProviderProps): ReactElement {
  const [enterprises, setEnterprises] = useState<IEnterprise[]>([]);
  const [firstSearch, setFirstSearch] = useState(true);

  const memoizedContextValue = useMemo<IEnterprisesContextData>(
    () => ({
      enterprises,
      setEnterprises,
      firstSearch,
      setFirstSearch,
    }),
    [enterprises, firstSearch],
  );

  return (
    <EnterprisesContext.Provider value={memoizedContextValue}>
      {children}
    </EnterprisesContext.Provider>
  );
}

export const useEnterprises = (): IEnterprisesContextData => {
  return useContext(EnterprisesContext);
};

export default EnterprisesProvider;
