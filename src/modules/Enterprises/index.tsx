import { Route, Routes } from 'react-router-dom';
import EnterprisesProvider from './hooks/EnterprisesContext';
import EnterpriseDetails from './pages/EnterpriseDetails';
import { EnterprisesSearch } from './pages/EnterprisesSearch';

export default function EnterprisesRoutes(): JSX.Element {
  return (
    <EnterprisesProvider>
      <Routes>
        <Route element={<EnterprisesSearch />} index />
        <Route element={<EnterpriseDetails />} path=":id" />
      </Routes>
    </EnterprisesProvider>
  );
}
