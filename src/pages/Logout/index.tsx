import { useEffect } from 'react';
import { useAuth } from '../../hooks/AuthContext';

export function Logout(): JSX.Element {
  const { handleLogout } = useAuth();

  useEffect(() => {
    handleLogout();
  }, [handleLogout]);

  return (
    <span>
      <h1>Saindo...</h1>
    </span>
  );
}

export default Logout;
