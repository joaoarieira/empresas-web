import { useCallback, useEffect, useState } from 'react';
import { useNavigate } from 'react-router-dom';
import { useFormik } from 'formik';
import * as Yup from 'yup';
import { AiOutlineUnlock } from 'react-icons/ai';
import { GoMail } from 'react-icons/go';

import {
  Container,
  LogoWrapper,
  WelcomeSubtitle,
  WelcomeTitle,
  CredentialsForm,
  CredentialsWarning,
} from './styles';
import logoImg from '../../assets/logo.png';
import { Button } from '../../components/Button';
import { Input } from '../../components/Input';
import { useAuth } from '../../hooks/AuthContext';

export function Login(): JSX.Element {
  const [incorrectPassword, setIncorrectPassword] = useState(false);

  const { authenticated, handleLogin } = useAuth();
  const navigate = useNavigate();

  const validationSchema = Yup.object().shape({
    email: Yup.string().email().required(),
    password: Yup.string().required(),
  });

  const loginForm = useFormik({
    enableReinitialize: true,
    validateOnBlur: false,
    validateOnChange: false,
    initialValues: {
      email: '',
      password: '',
    },
    validationSchema,
    onSubmit: async ({ email, password }) => {
      const success = await handleLogin(email, password);

      if (!success) setIncorrectPassword(true);
      else navigate('/enterprises');
    },
  });

  const handleChangeEmail = useCallback(
    ({
      target: { value },
    }: React.ChangeEvent<HTMLInputElement | HTMLTextAreaElement>) => {
      loginForm.setFieldValue('email', value);
      if (incorrectPassword) setIncorrectPassword(false);
    },
    [incorrectPassword, loginForm],
  );

  const handleChangePassword = useCallback(
    ({
      target: { value },
    }: React.ChangeEvent<HTMLInputElement | HTMLTextAreaElement>) => {
      loginForm.setFieldValue('password', value);
      if (incorrectPassword) setIncorrectPassword(false);
    },
    [incorrectPassword, loginForm],
  );

  useEffect(() => {
    if (authenticated) navigate('/enterprises');
  }, [authenticated, navigate]);

  return (
    <Container>
      <LogoWrapper>
        <img src={logoImg} alt="Logo" />
      </LogoWrapper>

      <WelcomeTitle>BEM-VINDO AO EMPRESAS</WelcomeTitle>

      <WelcomeSubtitle>
        Lorem ipsum dolor sit amet, contetur adipiscing elit. Nunc accumsan.
      </WelcomeSubtitle>

      <CredentialsForm onSubmit={loginForm.handleSubmit}>
        <Input
          startIcon={
            <GoMail
              style={{ padding: '3px 0px' }}
              size={26}
              color="var(--pink)"
            />
          }
          type="text"
          placeholder="E-mail"
          onChange={handleChangeEmail}
          value={loginForm.values.email}
          error={!!loginForm.errors.email || incorrectPassword}
          containerStyles={{ marginBottom: '2rem' }}
          autoFocus
        />

        <Input
          startIcon={<AiOutlineUnlock size={26} color="var(--pink)" />}
          type="password"
          placeholder="Senha"
          onChange={handleChangePassword}
          value={loginForm.values.password}
          error={!!loginForm.errors.password || incorrectPassword}
          containerStyles={{ marginBottom: '0.5rem' }}
        />

        <CredentialsWarning
          style={{ visibility: incorrectPassword ? 'visible' : 'hidden' }}
        >
          Credenciais informadas são inválidas, tente novamente.
        </CredentialsWarning>

        <Button
          type="submit"
          style={{ marginTop: '0.5rem', width: '100%' }}
          disabled={incorrectPassword}
        >
          Entrar
        </Button>
      </CredentialsForm>
    </Container>
  );
}
