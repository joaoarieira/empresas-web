import styled from 'styled-components';

export const Container = styled.div`
  display: flex;
  flex-direction: column;
  justify-content: center;
  align-items: center;

  padding: 2rem 0;

  min-height: calc(100vh - 2rem);
  margin: 0 auto;
  max-width: 22rem;
`;

export const LogoWrapper = styled.div`
  img {
    width: 100%;
    max-width: 285px;
  }
`;

export const WelcomeTitle = styled.h1`
  font-size: 1.5rem;
  width: 12rem;
  text-align: center;
  margin-top: 4rem;
  color: var(--grey-800);
`;

export const WelcomeSubtitle = styled.div`
  text-align: center;
  max-width: 90%;
  margin-top: 1.25rem;
  font-size: 1.125rem;
  color: var(--grey-800);
`;

export const CredentialsForm = styled.form`
  display: grid;
  grid-template-columns: 1fr;
  width: 100%;
  margin-top: 2.5rem;
`;

export const CredentialsWarning = styled.div`
  color: var(--neon-red);
  font-size: 0.8rem;
  font-family: Roboto, sans-serif;

  font-weight: normal;
  font-stretch: normal;
  font-style: normal;
  line-height: 1.95;
  letter-spacing: -0.17px;
  text-align: center;
`;
