import { lazy } from 'react';
import { Navigate, Route, Routes } from 'react-router-dom';
import { Login } from '../pages/Login';

const RequireAuth = lazy(() => import('./RequireAuth'));
const EnterprisesRoutes = lazy(() => import('../modules/Enterprises'));
const Logout = lazy(() => import('../pages/Logout'));

export function AppRoutes(): JSX.Element {
  return (
    <Routes>
      <Route index element={<Navigate to="/enterprises" />} />
      <Route path="logout" element={<Logout />} />
      <Route path="login" element={<Login />} />

      <Route
        path="enterprises/*"
        element={
          <RequireAuth>
            <EnterprisesRoutes />
          </RequireAuth>
        }
      />

      <Route path="*" element={<Navigate to="/" />} />
    </Routes>
  );
}

export default AppRoutes;
