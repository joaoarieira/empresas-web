import { createGlobalStyle } from 'styled-components';

export default createGlobalStyle`
  :root {
    // colors
    --grey-800: #383743;
    --grey-700: #403E4D;
    --grey-600: #748383;
    --grey-500: #8d8c8c;

    --beige: #EBE9D7;
    
    --pink: #EE4C77;
    
    --green: #7DC075;

    --greeny-blue: #57BBBC;

    --white: #FFFFFF;

    --indigo-900: #1A0E49;

    --neon-red: #FF0F44;

    --rouge: #991237;
  }

  * {
    margin: 0;
    padding: 0;
    outline: 0;
    box-sizing: border-box;
  }

  body {
    background: var(--light-grey);
    -webkit-font-smoothing: antialiased;
    -moz-osx-font-smoothing: grayscale;
    overflow-x: hidden;
  }

  body, input, button {
    font: 16px 'Roboto', sans-serif;
  }

  #root {
    position: relative;
    min-height: 100vh;
    background-color: var(--beige);
  }

  button {
    cursor: pointer;
  }

  h6, h5, h4, h3, h2, h1 {
    margin: 0;
  }
`;
