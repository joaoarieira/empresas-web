import { Suspense } from 'react';
import { Backdrop } from './components/Backdrop';
import AuthProvider from './hooks/AuthContext';
import HttpProvider from './hooks/HttpProvider';
import LoadingProvider from './hooks/LoadingContext';
import AppRoutes from './routes';

import GlobalStyle from './styles/global';

function App() {
  return (
    <Suspense fallback={<Backdrop />}>
      <LoadingProvider>
        <AuthProvider>
          <HttpProvider>
            <AppRoutes />
            <GlobalStyle />
          </HttpProvider>
        </AuthProvider>
        <Backdrop />
      </LoadingProvider>
    </Suspense>
  );
}

export default App;
