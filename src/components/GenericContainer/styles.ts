import styled from 'styled-components';

export const StyledContainer = styled.div`
  padding: 44px 50px;

  @media (max-width: 900px) {
    padding: 44px 20px;
  }
`;
