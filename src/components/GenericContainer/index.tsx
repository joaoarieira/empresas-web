import { StyledContainer } from './styles';

interface IGenericContainerProps {
  children?: React.ReactNode;
}

/** Container com paddings padrão, se comportando de modos diferentes para
 * cada tamanho de tela.
 */

export function GenericContainer({
  children,
}: IGenericContainerProps): JSX.Element {
  return <StyledContainer>{children}</StyledContainer>;
}
