import styled from 'styled-components';

export const StyledButton = styled.button`
  border-radius: 3.6px;
  height: 3.3rem;
  padding: 0.9rem;
  font-weight: bold;
  font-family: GillSans, 'Roboto', sans-serif;
  color: var(--white);
  text-transform: uppercase;
  border: none;
  background-color: var(--greeny-blue);

  transition: all ease 0.3s;

  &:disabled,
  &[disabled] {
    background-color: var(--grey-600);
    opacity: 56%;
  }
`;
