import { StyledButton } from './styles';

export function Button(
  props: React.ButtonHTMLAttributes<HTMLButtonElement>,
): JSX.Element {
  return <StyledButton type="button" {...props} />;
}
