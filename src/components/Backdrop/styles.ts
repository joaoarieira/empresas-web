import styled from 'styled-components';

export const SpinnerWrapper = styled.div`
  height: 100vh;
  width: 100vw;
  position: fixed;
  top: 0;
  left: 0;
  display: flex;
  justify-content: center;
  align-items: center;

  background-color: rgba(255, 255, 255, 0.6);

  & span {
    border-width: 8px;
  }
`;
