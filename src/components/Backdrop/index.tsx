/* eslint-disable react/jsx-no-useless-fragment */
import ReactDOM from 'react-dom';
import { ClipLoader } from 'react-spinners';
import { useLoading } from '../../hooks/LoadingContext';
import { SpinnerWrapper } from './styles';

/** Pode ser usado de qualquer lugar da aplicação. Para que este componente
 * seja mostrado, deve-se alterar o estado de loading através do método
 * setLoading disponível no contexto LoadingContext.
 */

export function Backdrop(): JSX.Element {
  const { loading } = useLoading();
  if (!loading) return <></>;

  const domElement = document.getElementById('portal');

  return domElement ? (
    ReactDOM.createPortal(
      <SpinnerWrapper>
        <ClipLoader
          speedMultiplier={0.5}
          size={132}
          color="var(--greeny-blue)"
        />
      </SpinnerWrapper>,
      domElement,
    )
  ) : (
    <></>
  );
}
