import { StyledHeader } from './styles';

/** Header padrão que pode ser utilizado em toda a aplicação.
 * Este componente serve apenas para "amarrar" o conteúdo do cabeçalho.
 */

export function Header(props: React.HTMLAttributes<HTMLElement>): JSX.Element {
  return <StyledHeader {...props} />;
}
