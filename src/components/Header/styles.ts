import styled from 'styled-components';

export const StyledHeader = styled.header`
  display: flex;
  align-items: center;
  width: 100%;
  height: 9.5rem;
  background: linear-gradient(
    180deg,
    rgba(234, 74, 117, 1) 0%,
    rgba(191, 65, 107, 1) 100%
  );
  padding: 0 2.5rem;

  @media (max-width: 900px) {
    height: 6rem;
  }
`;
