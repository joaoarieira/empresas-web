import styled from 'styled-components';

interface IInputContainer {
  error?: boolean;
}

export const InputContainer = styled.div<IInputContainer>`
  width: 100%;
  height: 36px;
  border-bottom: 1px solid
    ${props => (props.error ? 'var(--neon-red)' : 'var(--grey-800)')};
  display: flex;
  flex-wrap: nowrap;
`;

export const IconWrapper = styled.div`
  width: 2.375rem;

  button {
    background-color: transparent;
    border: none;
    width: 100%;
    height: 100%;
    display: flex;
    justify-content: center;
    align-items: center;
    padding-bottom: 0.5rem;
  }
`;

export const StyledInput = styled.input`
  width: 80%;
  height: 2rem;
  background-color: transparent;
  border: none;
  font-size: 1.25rem;
  letter-spacing: -0.31px;
  color: var(--grey-700);

  &::placeholder {
    font-size: 1.375rem;
    letter-spacing: -0.25px;
  }

  &:-webkit-autofill {
    -webkit-text-fill-color: var(--grey-700);
    background-color: black;
  }

  &:-webkit-autofill,
  &:-webkit-autofill:hover,
  &:-webkit-autofill:focus,
  &:-webkit-autofill:active {
    box-shadow: 0 0 0 30px var(--beige) inset !important;
    font-size: 1.25rem;
  }
`;
