import { RiErrorWarningFill } from 'react-icons/ri';
import { BsEye, BsEyeFill } from 'react-icons/bs';
import { useCallback, useState } from 'react';
import { IconWrapper, InputContainer, StyledInput } from './styles';

interface IInputProps extends React.InputHTMLAttributes<HTMLInputElement> {
  /** Renderiza um ícone ao lado mais à esquerda do input */
  startIcon?: React.ReactNode;

  /** Renderiza um ícone ao lado mais à direita do input */
  endIcon?: React.ReactNode;

  /** Caso true, o visual do componente é alterado. */
  error?: boolean;

  /** Use para estilizar o container que está em volta do input. */
  containerStyles?: React.CSSProperties;
}

/** Input estilizado com a funcionalidade de apresentar erro de validação. */

export function Input({
  startIcon,
  endIcon,
  error = false,
  type,
  containerStyles,
  ...props
}: IInputProps): JSX.Element {
  const [showPassword, setShowPassword] = useState(false);
  const [customHtmlType, setCustomHtmlType] = useState(type);

  const toggleShowPassword = useCallback(() => {
    setCustomHtmlType(!showPassword ? 'text' : 'password');
    setShowPassword(prev => !prev);
  }, [showPassword]);

  return (
    <InputContainer error={error} style={containerStyles}>
      {startIcon && <IconWrapper>{startIcon}</IconWrapper>}

      <StyledInput type={customHtmlType} {...props} />

      {endIcon && <IconWrapper>{endIcon}</IconWrapper>}

      {type === 'password' && (
        <IconWrapper>
          <button onClick={toggleShowPassword} type="button">
            {showPassword ? (
              <BsEye size={20} color="var(--grey-600)" strokeWidth="0.3px" />
            ) : (
              <BsEyeFill size={20} color="var(--grey-600)" />
            )}
          </button>
        </IconWrapper>
      )}

      {error && (
        <IconWrapper>
          <RiErrorWarningFill size={24} color="var(--neon-red)" />
        </IconWrapper>
      )}
    </InputContainer>
  );
}
